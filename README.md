# Latex2Anki

Scripts to convert and version your latex files with anki.

Current to-do:
- [ ] Add documentation in code
- [X] Add how-to-use 
- [X] Add project creation functionality

# What you can do with it
  - Convert latex with a specific structure into anki notes
  - Keep those notes up-to-date with latex document
  - Keep history of changes 

# What you need 
1.  Latex document(s) with Q/A format:

This what you need to have at the beginnig of you latex document
```
\newcommand{\question}[4]{\Large Q \vline\ \normalsize #2 (#3) \newline \newline \textcolor{answer_color}{ A  \vline \normalsize \ #4}
```

And every questions should have following structure: 
```
\question{1} % question_id (should be unique (can be text))
	{What is angstrom? How much atoms are in radius?} % question text
	{p1-4\cite{feynman1}}  % cite source (not required)
	{The atoms are 1 or 2$\times 10^{-8}$cm in radius. $10^{-8}$ cm is called \textit{angstrom} (\AA)} % answer text (not required, but is desirable)
        
```

2. Anki desktop application with created deck with which to sync

# How-To
0. Make sure to **change anki package version** in requirements.txt to match your anki application. 
1. Create virtual environment and install requirements

```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

2. Change config
Create config.ini in project root directory. You can use config_example.ini for guidence.

**sqlite_db_path*** = path to sqlite file that will be used for version controlling
you can use example_database.db 
**latex_files_directory** = path to directory with .tex files. 
You can create directory in project root and symlink all .tex files you want to sync with anki. 
Example: ln -s /path/to/tex/physics.tex physics.tex 
This way every time you change, for example, physics.tex in /path/to/tex the scipt will see it. 
**collection_path** = path you your local anki collection

3. (optional) type ```python start.py``` help to get overview of possible commands
4. Put files or symlinks (preferable) into **latex_files_directory**
5. Create project by typing ```python start.py new_project```
Project, basically, allows anki decks to be mapped to multiple files in **latex_files_directory**
6. Stage changes ```python start.py stage```. This will not make changes to your anki collection, but will **stage** all the changes to be commited. 
7. Commit changes ```python start.py commit```

