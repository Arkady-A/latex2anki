import pandas as pd
import re
from .data_retrival import get_last
import numpy as np
import logging
def parse_questions(file_path):
    # open file
    f = open(file_path, mode='r')
    lines = f.readlines()
    # check that file not empty
    if len(lines) == 0:
        print('File {} is empty, skipping'. format(full_filepath))
        return None
    # request a command to search for
    print('Print a command to parse from file ({}) or press [ENTER] to use default'.format(
        file_path))
    command = input('[\\question{]:>')
    if command =='':
        command = r'\question{'
    # parse questions from file
    questions = pd.DataFrame(find_questions(lines, command))
    return questions

def compare_with_last_version(questions, connection, project_id):
    # get last version and pick only questions
    # from one project
    lasts = get_last(connection)
    lasts = lasts.loc[lasts.project_id == project_id]
    # if there's no last questions with this parameters
    # all questions are new and should be updated
    if lasts.shape[0] == 0:
        questions.loc[:,'update']=True
        questions.loc[:,'anki_id']=None
        questions.loc[:,'version']=0
        questions.loc[:,'commited']=0
        return questionsprint(lasts)
    def compare(row):
        # find old question and if not found return new question for update
        logging.debug('''in {}: compare_with_last_version: processing row\n{}'''.format(__name__, row.to_string()))
        old_question = lasts.loc[lasts.question_id == row.question_id,:]
        logging.debug('''in {}: compare_with_last_version: old_question to compare:\n{}'''.format(
            __name__, old_question.to_string()))
        if old_question.empty:
            row.loc['update']=True
            row.loc['anki_id']=None
            row.loc['commited']=0
            row.loc['version']=0
            return row
        old_question = old_question.squeeze()
        qt_eq = (old_question['question_text'] == row.loc['question_text'])
        an_eq = (old_question['answer_text'] == row.loc['answer_text'])
        ci_eq = (old_question['cite'] == row.loc['cite'])
        logging.debug('''in {}: compare_with_last_version:
        Comparing 2 questions
        {} =? {} : {}
        {} =? {} : {}
        {} =? {} : {}
        '''.format(__name__,
                   old_question['question_text'], row.loc['question_text'], qt_eq,
                   old_question['answer_text'], row.loc['answer_text'], an_eq,
                   old_question['cite'], row.loc['cite'], ci_eq))
       # print('\n\n\n')
       # print(qt_eq, an_eq, ci_eq)
       # print(old_question)
       # print(row)
        if np.array([qt_eq, an_eq, ci_eq]).all():
            row.loc['update']=False
            row.loc['anki_id']=None
            row.loc['commited']=0
            row.loc['version']=-1
            return row
            old_question.loc['update']=False
           # print('return old', old_question)
            return old_question
        else:
            row.loc['anki_id']=old_question.loc['anki_id']
            row.loc['commited']=0
            row.loc['update']=True
            row.loc['version']=old_question.loc['version']
            #print('new_row', row)
            return row
    questions = questions.apply(compare,axis=1)
    return questions
    pass

def anki_latex_converter(row):
    ans_text = row.loc['answer_text']
    qst_text = row.loc['question_text']
    pattern_1 = r'(\${1,2})([^$]*\${1,2}|)'
    pattern_2 = r'(\[latex\][^$]*)(\${1,2}|)'

    
    ans_text = re.sub(pattern_1, r'[latex]\1\2[/latex]', ans_text)    
    qst_text = re.sub(pattern_1, r'[latex]\1\2[/latex]', qst_text)
    
    row.loc['answer_text']=ans_text
    row.loc['question_text']=qst_text
    return row

def find_questions(lines, keyword):
    '''
    Will find questions in lines 

    :param lines: Lines of document
    :type lines: list
    :param keyword: Keyword to look for (e.g. "\question{")
    :type keywork: str 
    :return: Dictionary with Question_id, Question_text, cite, answer_text
    :rtype: dict
    '''
    buff = []
    sb = 0
    result = {
        'question_id':[],
        'question_text':[],
        'cite':[],
        'answer_text':[],
    }
    order = list(result.keys())
    o = 0
    for line_index, line in enumerate(lines):
        o = 0
        if keyword in line:
#             print(line)
            s = None
            s_li = None
            e = None
            e_li = None
#             print('RESULT',result)
            while o < 4:
                for symbol_index, symbol in enumerate(line):
#                 if symbol in ['{', '}']:
                    if symbol == '{':
#                         print(o,s,e,sb,line)
                        if sb == 0:
                            s = symbol_index+1
                            s_li = line_index
                        sb+=1
                    if symbol == '}':
                        sb-=1
                        if sb == 0:
                            e = symbol_index
                            e_li = line_index
                            if e_li == s_li:
                                result[order[o]].append(line[s:e])
                            else:
                                buff_o = ''
                                for i in range(s_li, e_li+1):
                                    addt=''
                                    if i == s_li:
                                        addt=(lines[i][s:])
                                    elif i == e_li:
                                        addt=(lines[i][:e])
                                    else:
                                        addt=lines[i]
                                    buff_o+=addt
                                result[order[o]].append(buff_o)
                            s= None
                            e= None
                            s_li = None
                            e_li = None
                            o += 1
                line_index += 1 
                line = lines[line_index]
    return result
