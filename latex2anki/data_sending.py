import sqlite3
import anki
import pandas as pd
import numpy as np
from anki import Collection

def save_changes_to_questions(questions, connection, check_for_update=True):

    if check_for_update==True:
        questions = questions.loc[questions.loc[:,'update']==True,:]
    cols_to_save = ['question_id','project_id','version','question_text',
                    'answer_text','cite','commited','anki_id']
    questions = questions.loc[:, cols_to_save]
    questions.version = questions.version + 1
    questions.to_sql('questions', connection, if_exists='append', index=False)
    return True

def commit_to_anki(questions, collection_path, connection):
    def update_question(row):
        anki_id = row['anki_id']
        question_text = row['question_text']
        answer_text = row['answer_text']
        cite = row['cite']
        commited = row['commited']
        deck_id = int(row['anki_deck_id'])

        col = Collection(collection_path)

        if np.isnan(anki_id):
            #### New note addition process ####
            new_note = col.newNote()
            new_note.fields = [question_text, answer_text]
            new_note.addTag('generated_automatically')
            note_types = col.db.execute('select id,name from notetypes')
            for note_type in note_types:
                if str.lower(note_type[1]) == 'basic':
                    note_type_id = note_type[0]
                    break
            new_note.mid = note_type_id
            col.add_note(new_note, deck_id)
            new_anki_id = col.db.execute('select max(id) from notes')[0][0]
            col.close(save=True)
            #### To be update in local database ####
            row.loc['anki_id']=new_anki_id
            row.loc['commited']=1
        else:
            sep = '\x1f'
            new_flds = sep.join([question_text, answer_text])
            if cite:
                new_flds+='<br>Cite: "{}"'.format(cite)
                col.db.execute('UPDATE notes SET flds=? WHERE id=?;', new_flds, anki_id) # this is ok
                row['commited'] = 1
                col.close(save=True)
        return row
    questions = questions.apply(update_question, axis=1)
    return questions

def create_new_project(local_project_name, deck_id, anki_deck_name, connection):
    '''Creates entry in projects table'''
    print(connection)
    sql_get = 'SELECT * FROM projects'
    idn = pd.read_sql(sql_get, connection).loc[:,'id'].max()+1
    sql_put = r'INSERT INTO projects VALUES (?, ?, ?, ?)'
    curs = connection.cursor()
    for el in [idn, local_project_name, deck_id, anki_deck_name]:
        print(el, type(el))
    curs.execute(sql_put, (int(idn), local_project_name, str(deck_id), anki_deck_name))
    connection.commit()
    pass
