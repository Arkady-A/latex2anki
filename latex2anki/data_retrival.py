import sqlite3
import anki
import pandas as pd
import logging

def get_project_id_by_filename(filename, connection):
    c= connection.cursor()
    c.execute('SELECT * FROM filename_associations WHERE filename LIKE ?', (filename,))
    results = c.fetchall()
    if len(results) == 0:
        print("{} file has no association with any project. \
\nChoose a project to associate this filename with (id)".format(filename))
        all_project_df = pd.read_sql_query("SELECT * FROM projects", connection)
        print(all_project_df)
        project_id = input(':>')
        connection.execute(''' INSERT INTO filename_associations
        (project_id, filename) VALUES (?,?)''',(project_id, filename))
        connection.commit()
        print("Association added")
    elif len(results)>1:
        all_project_df = pd.read_sql_query("SELECT * FROM projects WHERE filename LIKE ?", conn, params=(filename,))
        print('Several possible projects. Type project id (from project_id column):')
        print(all_project_df)
        project_id = input(':>')
    else:
        project_id = results[0][1]
    c.close()
    return project_id

def get_last(connection):

    sql_query = '''
    SELECT *
    FROM(
    SELECT
        id,
        project_id,
        question_id,
        version,
        question_text,
        answer_text,
        cite,
        commited,
        anki_id
    FROM
    questions
    GROUP BY
        question_id, project_id
    HAVING version = MAX(version)
    ) as df
    INNER JOIN projects ON
    df.project_id = projects.id
    '''
    last_versions_for_all_projects = pd.read_sql(sql_query, connection)
    logging.debug('in {}: get_last: found\n {} \nas lasts'.format(__name__, last_versions_for_all_projects.to_string(max_colwidth=20)))
    return last_versions_for_all_projects

def get_last_not_commited(connection):
    last_versions = get_last(connection)
    return last_versions.loc[last_versions.commited==0]


def load_all_decks(connection):
    sql_query = '''
    SELECT *
    FROM decks
    WHERE
    id != 1
    '''
    all_decks = pd.read_sql(sql_query, connection)
    return all_decks
    pass

