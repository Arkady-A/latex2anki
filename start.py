# for parsing configs
import configparser
import logging
import latex2anki
import sys
import pandas as pd
import os
import sqlite3
import latex2anki

config = configparser.ConfigParser()
config.read_file(open('config.ini', mode='r'))

LOGFILE_PATH = config['DEBUG']['log_file_path']
COLLECTION_PATH = config['ANKI']['collection_path']
SQLITE_DB_PATH = config['LOCAL']['sqlite_db_path']
LATEX_FILES_DIRECTORY = config['LOCAL']['latex_files_directory']

logging.basicConfig(filename=LOGFILE_PATH, encoding='utf-8', level=logging.DEBUG, format='%(asctime)s %(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

print('Config file found and loaded \nAnki collection path: {}\nSQLITE path: {}\nDirectory with LaTex (.tex) files: {} '.format(
    COLLECTION_PATH,
    SQLITE_DB_PATH,
    LATEX_FILES_DIRECTORY))

# handle possible issues
if not os.path.exists(COLLECTION_PATH):
    raise ValueError(
        'ANKI collection not found(path:{}). Please, adjust your config file.'.format(COLLECTION_PATH))

if not os.path.exists(SQLITE_DB_PATH):
    raise ValueError(
        'SQLITE database not found(path:{}). Please, adjust your config file.'.format(SQLITE_DB_PATH))

if not os.path.exists(LATEX_FILES_DIRECTORY):
    raise ValueError('LATEX directory not found(path:{}). Please, adjust your config file.'.format(
        LATEX_FILES_DIRECTORY))

print('All files are found!')
if len(sys.argv) == 1:
    raise InputError("No command provided")
# handle different commands
command = sys.argv[1]
if command == 'help':
    print(
        '''Avaliable commands:
        stage - stage all update in *.tex files stored in "{}" folder
        commit - commit stages changes to anki collection "{}"
        new_project - creates new local project record (refer to documentation)
        help - shows this help message
        '''.format(LATEX_FILES_DIRECTORY, COLLECTION_PATH))
elif command == 'stage':
    connection = sqlite3.connect(SQLITE_DB_PATH)
    logging.info('Established connection to local sql base')
    logging.debug('DBPATH:{}'.format(SQLITE_DB_PATH))
    try:
        filenames = os.listdir(LATEX_FILES_DIRECTORY)
        logging.debug('Filelist:{}'.format(filenames))
        for filename in filenames:
            if not( '.tex' in filename):
                print('filename {} in the directory has bad format (".tex" not in filename'. format
                      (filename))
                continue
            associated_project_id = latex2anki.get_project_id_by_filename(filename, connection)
            logging.debug('For file {} found associated project with id {}'.format(filename, associated_project_id))
            # process file
            file_path = os.path.join(LATEX_FILES_DIRECTORY, filename)
            parsed_questions = latex2anki.parse_questions(file_path)
            logging.debug('For file {} parsed questions:\n{}'.format(file_path, parsed_questions.to_string()))
            if parsed_questions is None:
                raise ValueError('File () is empty. Skipping'.format(filename))
            # latex formulas to anki 
            parsed_questions = parsed_questions.apply(latex2anki.anki_latex_converter, axis=1)

            logging.debug('Convert LaTEX formulas for anki\n{}\n'.format(parsed_questions.to_string()))
            # update in local storage
            parsed_questions.loc[:,'project_id'] = associated_project_id
            logging.info('File {} parsed and ready to be written in local db'.format(filename))
            parsed_questions = latex2anki.compare_with_last_version(
                parsed_questions, connection, associated_project_id)
            logging.debug('Comparing questions returned\n{}\n'.format(parsed_questions.to_string()))
            print(parsed_questions)
            results = latex2anki.save_changes_to_questions(parsed_questions, connection)
            logging.debug('Writing to local database returned:\n{}'.format(results))
            # print results
            print("Project id: {}".format(associated_project_id), 'results:',sep='\n')
            print(results)
    except Exception as e:
        connection.close()
        print('Got exception! Connection to database safely closed.')
        raise(e)
    connection.close()
    
elif command == 'commit':
    connection = sqlite3.connect(SQLITE_DB_PATH)
    not_commited = latex2anki.get_last_not_commited(connection)
    print('\n\n\nNOTCOMMITED\n',not_commited)
    commited = latex2anki.commit_to_anki(not_commited, COLLECTION_PATH, connection)
    print(commited)
    results = latex2anki.save_changes_to_questions(commited, connection, check_for_update=False)
    print("Result of commitment")
    print(results)
    connection.close()

elif command == 'new_project':
    connection_local = sqlite3.connect(SQLITE_DB_PATH)
    connection_anki = sqlite3.connect(COLLECTION_PATH)

    # give time for user to create a deck in anki
    print("[Optional] Create new deck in anki app and close anki window")
    input("Press [ENTER] to proceed")
    all_decks = latex2anki.load_all_decks(connection_anki)

    print("Choose one of the decs for which new project will be created")
    print(all_decks.loc[:,'name'])
    pr_id = int(input(":>"))
    if pr_id>all_decks.shape[0]:
        print('Id is not in range!')
        while pr_id>all_decks.shape[0]:
            pr_id = int(input(":>"))
    deck_name = all_decks.iloc[pr_id].loc['name']
    # get local project name 
    print("Type name for new LOCAL project in sql base")
    local_project_name = input('[{}]:>'.format(deck_name))
    if len(local_project_name)==0:
        local_project_name = deck_name

    # get deck id
    deck_id = all_decks.iloc[pr_id,:].loc['id']
    result = latex2anki.create_new_project(local_project_name, deck_id, deck_name, connection_local)
    connection_anki.close()
    connection_local.close()
    print('Project {} associated with anki\'s deck with id "{}" with name {}'.format(local_project_name, deck_id, deck_name))

    pass

else:
    print('Command "{}" invalid. Use Help for more information'.format(command))
